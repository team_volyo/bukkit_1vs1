package me.jack30t.one_vs_one;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.block.BlockState;
import org.bukkit.block.Sign;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntitySpawnEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.*;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;
import java.util.HashMap;
/*
 * Created by StAn on 01.09.2015. Now with lovely help by smuuuuuuu -smuuuuuuu 09.09.2015 21:32
 */
public class Main extends JavaPlugin implements Listener {
    HashMap<Player, Player> request = new HashMap<>();
    HashMap<Player, Boolean> inmatch = new HashMap<>();
    HashMap<String, Location> arenalistp1 = new HashMap<>();
    HashMap<String, Location> arenalistp2 = new HashMap<>();
    HashMap<String, Boolean> arenaused = new HashMap<>();
    HashMap<Player, String> currentarena = new HashMap<>();
    HashMap<Player, Integer> arenapos = new HashMap<>();
    HashMap<Player, Boolean> movingallowed = new HashMap<>();
    World onevsonelobby;
    World spawnworld;
    ItemStack diaweapon;
    ItemMeta swordName;
    Float yaw1;
    Float pitch1;
    Float yaw2;
    Float pitch2;
    Location spawnworldloc;
    Location onevsone;
    int arenacount;
    @Override
    public void onEnable() {
        onevsonelobby = Bukkit.getWorld(this.getConfig().getString("1o1.coords.spawn.world"));
        spawnworld = Bukkit.getWorld(this.getConfig().getString("1o1.coords.spawnworld.world"));
        diaweapon = new ItemStack(Material.DIAMOND_SWORD);
        swordName = diaweapon.getItemMeta();
        yaw1 = Float.parseFloat(this.getConfig().getString("1o1.coords.spawn.yaw"));
        pitch1 = Float.parseFloat(this.getConfig().getString("1o1.coords.spawn.pitch"));
        yaw2 = Float.parseFloat(this.getConfig().getString("1o1.coords.spawnworld.yaw"));
        pitch2 = Float.parseFloat(this.getConfig().getString("1o1.coords.spawnworld.pitch"));
        spawnworldloc = new Location(spawnworld, this.getConfig().getDouble("1o1.coords.spawnworld.x"), this.getConfig().getDouble("1o1.coords.spawnworld.y"), this.getConfig().getDouble("1o1.coords.spawnworld.z"), yaw2, pitch2);
        onevsone = new Location(onevsonelobby, this.getConfig().getDouble("1o1.coords.spawn.x"), this.getConfig().getDouble("1o1.coords.spawn.y"), this.getConfig().getDouble("1o1.coords.spawn.z"), yaw1, pitch1);
        swordName.setDisplayName(ChatColor.AQUA + "1o1 Inviter");
        diaweapon.setItemMeta(swordName);
        for (Player p : Bukkit.getServer().getOnlinePlayers()) {
            inmatch.put(p, false);
            currentarena.put(p, "");
            movingallowed.put(p, true);
        }
        arenacount = this.getConfig().getConfigurationSection("1o1.arenas").getKeys(false).size();
        int i;
        for (i = 1; i <= arenacount; i = i + 1) {
            arenaused.put("arena" + i, false);
        }
        int e;
        for (e = 1; e <= arenacount; e = e + 1) {
            String arena = "arena" + e;
            World onevsonearena = Bukkit.getServer().getWorld(this.getConfig().getString("1o1.arenas." + arena + ".1.world"));
            Float yaw1 = Float.parseFloat(this.getConfig().getString("1o1.arenas." + arena + ".1.yaw"));
            Float pitch1 = Float.parseFloat(this.getConfig().getString("1o1.arenas." + arena + ".1.pitch"));
            Float yaw2 = Float.parseFloat(this.getConfig().getString("1o1.arenas." + arena + ".2.yaw"));
            Float pitch2 = Float.parseFloat(this.getConfig().getString("1o1.arenas." + arena + ".2.pitch"));
            Location pos1 = new Location(onevsonearena, this.getConfig().getDouble("1o1.arenas." + arena + ".1.x"), this.getConfig().getDouble("1o1.arenas." + arena + ".1.y"), this.getConfig().getDouble("1o1.arenas." + arena + ".1.z"), yaw1, pitch1);
            Location pos2 = new Location(onevsonearena, this.getConfig().getDouble("1o1.arenas." + arena + ".2.x"), this.getConfig().getDouble("1o1.arenas." + arena + ".2.y"), this.getConfig().getDouble("1o1.arenas." + arena + ".2.z"), yaw2, pitch2);
            arenalistp1.put(arena, pos1);
            arenalistp2.put(arena, pos2);
        }
        Bukkit.getServer().getPluginManager().registerEvents(this, this);
    }
    @Override
    public void onDisable() {
    }
    @EventHandler
    public void onMove(PlayerMoveEvent event) {
        if (movingallowed.get(event.getPlayer()) == false) {
            if (arenapos.get(event.getPlayer()) == 1) {
                event.getPlayer().teleport(arenalistp1.get(currentarena.get(event.getPlayer())));
            } else if (arenapos.get(event.getPlayer()) == 2) {
                event.getPlayer().teleport(arenalistp2.get(currentarena.get(event.getPlayer())));
            }
        }
    }
    @EventHandler
    public void BecomeDamageEvent(EntityDamageByEntityEvent event) {
        if (event.getDamager() instanceof Player && event.getEntity() instanceof Player) {
            Player p = ((Player) event.getDamager()).getPlayer();
            if (!inmatch.get(p)) {
                if (((Player) event.getDamager()).getItemInHand().getType() == Material.DIAMOND_SWORD && ((Player) event.getDamager()).getItemInHand().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.AQUA + "1o1 Inviter")) {
                    final Player player = (Player) event.getDamager();
                    final Player damaged = (Player) event.getEntity();
                    if (request.get(player) == damaged) {
                        damaged.sendMessage(ChatColor.GREEN + "The Player " + ChatColor.YELLOW + player.getName() + ChatColor.GREEN + " accepted your 1vs1 invite! Good Luck!");
                        player.sendMessage(ChatColor.GREEN + "You accepted the 1vs1 invite from " + ChatColor.YELLOW + damaged.getName() + ChatColor.GREEN + "! Good Luck!");
                        request.remove(player);
                        int i;
                        for (i = 1; i <= arenacount; i = i + 1) {
                            Boolean status = arenaused.get("arena" + i);
                            if (!status) {
                                String arena = "arena" + i;
                                currentarena.put(damaged, arena);
                                currentarena.put(player, arena);
                                damaged.getInventory().clear();
                                player.getInventory().clear();
                                arenapos.put(damaged, 1);
                                arenapos.put(player, 2);
                                damaged.teleport(arenalistp1.get(arena));
                                player.teleport(arenalistp2.get(arena));
                                arenaused.put(arena, true);
                                movingallowed.put(damaged, false);
                                movingallowed.put(player, false);
                                ItemStack armor1 = new ItemStack(Material.IRON_HELMET);
                                ItemStack armor2 = new ItemStack(Material.IRON_CHESTPLATE);
                                ItemStack armor3 = new ItemStack(Material.IRON_LEGGINGS);
                                ItemStack armor4 = new ItemStack(Material.IRON_BOOTS);
                                ItemStack weapon1 = new ItemStack(Material.STONE_SWORD);
                                ItemStack weapon2 = new ItemStack(Material.FISHING_ROD);
                                ItemStack weapon3 = new ItemStack(Material.BOW);
                                ItemStack arrows = new ItemStack(Material.ARROW, 5);
                                damaged.getInventory().setHelmet(armor1);
                                damaged.getInventory().setChestplate(armor2);
                                damaged.getInventory().setLeggings(armor3);
                                damaged.getInventory().setBoots(armor4);
                                damaged.getInventory().addItem(weapon1);
                                damaged.getInventory().addItem(weapon2);
                                damaged.getInventory().addItem(weapon3);
                                damaged.getInventory().setItem(9, arrows);
                                player.getInventory().setHelmet(armor1);
                                player.getInventory().setChestplate(armor2);
                                player.getInventory().setLeggings(armor3);
                                player.getInventory().setBoots(armor4);
                                player.getInventory().addItem(weapon1);
                                player.getInventory().addItem(weapon2);
                                player.getInventory().addItem(weapon3);
                                player.getInventory().setItem(9, arrows);
                                inmatch.put(damaged, true);
                                inmatch.put(player, true);
                                int countdown;
                                int high = 0;
                                player.setLevel(4);
                                int TaskID = Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(this, new Runnable() {
                                    public void run() {
                                        player.playSound(player.getLocation(), Sound.LEVEL_UP, 10, 1);
                                        damaged.playSound(damaged.getLocation(), Sound.LEVEL_UP, 10, 1);
                                        movingallowed.put(player, true);
                                        movingallowed.put(damaged, true);
                                    }
                                }, 60L);
                                break;
                            }
                            if (i == arenacount) {
                                damaged.sendMessage(ChatColor.DARK_RED + "Sorry, there is no free Arena... Please try again later!");
                                player.sendMessage(ChatColor.DARK_RED + "Sorry, there is no free Arena... Please try again later!");
                                break;
                            }
                        }
                    } else if (request.get(damaged) == player) {
                        player.sendMessage(ChatColor.RED + "You've already invited " + ChatColor.YELLOW + damaged.getName() + ChatColor.RED + ".");
                    } else {
                        request.put(damaged, player);
                        player.sendMessage(ChatColor.DARK_AQUA + "You invited " + ChatColor.YELLOW + damaged.getName() + ChatColor.DARK_AQUA + " for an 1vs1 Fight!");
                        damaged.sendMessage(ChatColor.DARK_AQUA + "You got invited by " + ChatColor.YELLOW + player.getName() + ChatColor.DARK_AQUA + "! Please punch him/her with your Diamond Sword!");
                    }
                    event.setCancelled(true);
                }
            }
        }
    }
    @EventHandler
    public void onDropItem(PlayerDropItemEvent event) {
        event.setCancelled(true);
    }
    @EventHandler
    public void onEntitySpawn(EntitySpawnEvent event) {
        if (event.getEntity() instanceof Item) {
            event.setCancelled(true);
        }
    }
    @EventHandler
    public void onDeath(PlayerDeathEvent event) {
        if (event.getEntity() instanceof Player) {
            if (event.getEntity().getLastDamageCause().getCause() != EntityDamageEvent.DamageCause.FALL) {
                Player player = event.getEntity();
                final String spieler = player.getKiller().getName();
                if (inmatch.get(player)) {
                    event.setDeathMessage(null);
                    inmatch.put(player.getKiller(), false);
                    arenaused.put(currentarena.get(player), false);
                    currentarena.remove(player);
                    currentarena.remove(player.getKiller());
                    player.getKiller().sendMessage(ChatColor.GREEN + "You won! :) We teleport you back to the 1o1 Lobby now!");
                    double hearts = Math.ceil(player.getKiller().getHealth()) / 2;
                    player.sendMessage(ChatColor.RED + "You lost this Game! Your Opponent had " + ChatColor.YELLOW + hearts + " " + '\u2764' + ChatColor.RED + " left. Hopefully you have more Luck in the next Game!");
                    event.getDrops().clear();
                    new BukkitRunnable() {
                        @Override
                        public void run() {
                            Bukkit.broadcastMessage(ChatColor.DARK_RED + spieler);
                            Player p = Bukkit.getPlayer(spieler);
                            p.teleport(onevsone);
                            p.setHealth(20.0f);
                            p.getInventory().clear();
                            p.getInventory().setHelmet(new ItemStack(Material.AIR));
                            p.getInventory().setChestplate(new ItemStack(Material.AIR));
                            p.getInventory().setLeggings(new ItemStack(Material.AIR));
                            p.getInventory().setBoots(new ItemStack(Material.AIR));
                            p.getInventory().addItem(diaweapon.clone());
                        }
                    }.runTaskLater(this, 60);
                }
            }
        }
        }
    @EventHandler
    public void onRespawn(PlayerRespawnEvent event) {
                if (inmatch.get(event.getPlayer())){
                    Player respawned = event.getPlayer();
                    inmatch.put(respawned, false);
                }
        event.getPlayer().getInventory().addItem(diaweapon.clone());
    }
    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        event.getPlayer().getInventory().clear();
        inmatch.put(event.getPlayer(), false);
        currentarena.put(event.getPlayer(), "");
        movingallowed.put(event.getPlayer(), true);
        event.getPlayer().teleport(spawnworldloc);
    }
    @EventHandler
    public void onQuit(PlayerQuitEvent event) {
        event.setQuitMessage(null);
    }
    @EventHandler
    public void onChat(AsyncPlayerChatEvent event) {
        event.setMessage(ChatColor.WHITE + event.getMessage());
    }
    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent e) {
        if (e.getAction().equals(Action.RIGHT_CLICK_BLOCK)) {
            Bukkit.broadcastMessage("test3");
            Block i = e.getClickedBlock();
            Bukkit.broadcastMessage("test4");
            if (i.getState() instanceof Sign) {
                Bukkit.broadcastMessage("test5");
                BlockState stateBlock = i.getState();
                Bukkit.broadcastMessage("test6");
                Sign sign = (Sign) stateBlock;
                Bukkit.broadcastMessage("test7");
                if (sign.getLine(0).equalsIgnoreCase(ChatColor.AQUA + "1VS1")) {
                    Player p = e.getPlayer();
                    Bukkit.broadcastMessage("test8");
                    p.getInventory().addItem(diaweapon);
                    Bukkit.broadcastMessage("test9" + p.getName());
                    p.teleport(onevsone);
                    Bukkit.broadcastMessage("test10");
                } else if (sign.getLine(0).equalsIgnoreCase(ChatColor.AQUA + "Spawn")) {
                    Player p = e.getPlayer();
                    Bukkit.broadcastMessage("test11");
                    p.getInventory().clear();
                    Bukkit.broadcastMessage("test12");
                    p.teleport(spawnworldloc);
                    Bukkit.broadcastMessage("test13");
                }
            }
        }
    }
    public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
        Player player = (Player) sender;
        if (cmd.getName().equalsIgnoreCase("oooaddarena")) {
            this.getConfig().set("1o1.arenas." + args[0] + "." + args[1] + ".x", player.getLocation().getX());
            this.getConfig().set("1o1.arenas." + args[0] + "." + args[1] + ".y", player.getLocation().getY());
            this.getConfig().set("1o1.arenas." + args[0] + "." + args[1] + ".z", player.getLocation().getZ());
            this.getConfig().set("1o1.arenas." + args[0] + "." + args[1] + ".world", player.getLocation().getWorld().getName());
            this.getConfig().set("1o1.arenas." + args[0] + "." + args[1] + ".yaw", player.getLocation().getYaw());
            this.getConfig().set("1o1.arenas." + args[0] + "." + args[1] + ".pitch", player.getLocation().getPitch());
            player.sendMessage(ChatColor.GREEN + "Position " + args[1] + " in " + args[0] + " was successfully added!");
            ArmorStand stand = player.getLocation().getWorld().spawn(player.getLocation(), ArmorStand.class);
            stand.setCustomNameVisible(true);
            stand.setCustomName(ChatColor.GREEN + "Arena: " + args[0] + " PlayerSpawnPoint: " + args[1]);
            this.saveConfig();
            this.reloadConfig();
            arenacount = this.getConfig().getConfigurationSection("1o1.arenas").getKeys(false).size();
            return true;
        } else if (cmd.getName().equalsIgnoreCase("ooosetspawn")) {
            this.getConfig().set("1o1.coords.spawn.x", player.getLocation().getX());
            this.getConfig().set("1o1.coords.spawn.y", player.getLocation().getY());
            this.getConfig().set("1o1.coords.spawn.z", player.getLocation().getZ());
            this.getConfig().set("1o1.coords.spawn.world", player.getLocation().getWorld().getName());
            this.getConfig().set("1o1.coords.spawn.yaw", player.getLocation().getYaw());
            this.getConfig().set("1o1.coords.spawn.pitch", player.getLocation().getPitch());
            this.saveConfig();
            this.reloadConfig();
            player.sendMessage(ChatColor.GREEN + "1o1 Lobbyspawn successfully placed!");
            return true;
        } else if (cmd.getName().equalsIgnoreCase("ooosetworldspawn")) {
            this.getConfig().set("1o1.coords.spawnworld.x", player.getLocation().getX());
            this.getConfig().set("1o1.coords.spawnworld.y", player.getLocation().getY());
            this.getConfig().set("1o1.coords.spawnworld.z", player.getLocation().getZ());
            this.getConfig().set("1o1.coords.spawnworld.world", player.getLocation().getWorld().getName());
            this.getConfig().set("1o1.coords.spawnworld.yaw", player.getLocation().getYaw());
            this.getConfig().set("1o1.coords.spawnworld.pitch", player.getLocation().getPitch());
            this.saveConfig();
            this.reloadConfig();
            player.sendMessage(ChatColor.GREEN + "Worldspawn successfully placed!");
            return true;
        } else if (cmd.getName().equalsIgnoreCase("spawn")) {
            player.teleport(spawnworldloc);
            player.sendMessage(ChatColor.YELLOW + "Teleporting you to Spawn..");
            return true;
        } else if (cmd.getName().equalsIgnoreCase("oooshowpositions")) {
            if (args[0].equalsIgnoreCase("on")) {
                for (Entity en : player.getWorld().getEntities()) {
                    if (en instanceof ArmorStand) {
                        if (en.getCustomName().contains(ChatColor.GREEN + "Arena: ") && en.getCustomName().contains(ChatColor.GREEN + "PlayerSpawnPoint: ")) {
                            en.remove();
                        }
                    }
                }
                int i;
                for (i = 1; i <= arenacount; i = i + 1) {
                    String arena = "arena" + i;
                    World onevsonearena = Bukkit.getServer().getWorld(this.getConfig().getString("1o1.arenas." + arena + ".1.world"));
                    Float yaw1 = Float.parseFloat(this.getConfig().getString("1o1.arenas." + arena + ".1.yaw"));
                    Float pitch1 = Float.parseFloat(this.getConfig().getString("1o1.arenas." + arena + ".1.pitch"));
                    Float yaw2 = Float.parseFloat(this.getConfig().getString("1o1.arenas." + arena + ".2.yaw"));
                    Float pitch2 = Float.parseFloat(this.getConfig().getString("1o1.arenas." + arena + ".2.pitch"));
                    Location pos1 = new Location(onevsonearena, this.getConfig().getDouble("1o1.arenas." + arena + ".1.x"), this.getConfig().getDouble("1o1.arenas." + arena + ".1.y"), this.getConfig().getDouble("1o1.arenas." + arena + ".1.z"), yaw1, pitch1);
                    Location pos2 = new Location(onevsonearena, this.getConfig().getDouble("1o1.arenas." + arena + ".2.x"), this.getConfig().getDouble("1o1.arenas." + arena + ".2.y"), this.getConfig().getDouble("1o1.arenas." + arena + ".2.z"), yaw2, pitch2);
                    ArmorStand stand = pos1.getWorld().spawn(pos1, ArmorStand.class);
                    stand.setCustomNameVisible(true);
                    stand.setCustomName(ChatColor.GREEN + "Arena: " + arena + " PlayerSpawnPoint: " + 1);
                    ArmorStand stand1 = pos2.getWorld().spawn(pos2, ArmorStand.class);
                    stand1.setCustomNameVisible(true);
                    stand1.setCustomName(ChatColor.GREEN + "Arena: " + arena + " PlayerSpawnPoint: " + 2);
                }
                player.sendMessage(ChatColor.GREEN + "ArmorStands successfully spawned! :)");
            } else if (args[0].equalsIgnoreCase("off")) {
                for (Entity en : player.getWorld().getEntities()) {
                    if (en instanceof ArmorStand) {
                        if (en.getCustomName().contains(ChatColor.GREEN + "Arena: ") && en.getCustomName().contains(ChatColor.GREEN + "PlayerSpawnPoint: ")) {
                            en.remove();
                        }
                    }
                }
                player.sendMessage(ChatColor.GREEN + "ArmorStands successfully killed! :)");
            } else {
                player.sendMessage(ChatColor.DARK_RED + "Wrong usage! Use /oooshowpositions [on/off]");
            }
            return true;
        }
        return false;
    }
}
